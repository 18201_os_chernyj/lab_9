#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>

#define CHILD_PID 0
#define MIN_ARGC 2

int main(int argc, char ** argv) {
    if (argc < MIN_ARGC) {
        perror("ERROR: Not enough arguements\n");
        return EXIT_FAILURE;
    }

    pid_t pid = fork();

    if (pid == -1) {
        perror("ERROR: error in fork()\n");
        return EXIT_FAILURE;
    }

    if (pid == CHILD_PID) {
        execl("/bin/cat", "cat", argv[1], (char *)NULL);
        perror("ERROR: error in cat()\n");
        return EXIT_FAILURE;

    }
    int wait_status;
    if (wait(&wait_status) == -1)
    {
        perror("ERROR: error in wait()\n");
        return EXIT_FAILURE;
    }
    printf("\nOutput from parent process\n");

    return EXIT_SUCCESS;
}
